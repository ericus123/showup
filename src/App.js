import "./App.css";
import "antd/dist/antd.css";
import "./styles/scss/project_theme.scss";

import Routes from "./routes";

function App() {
  return <Routes/>;
}

export default App;
