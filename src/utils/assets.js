import red_logo from "../assets/images/red_icon.png";
import homepage_img from "../assets/images/newevent.png";
import event_image from "../assets/images/event-img.png";

export const assets = {
    red_logo,
    homepage_img,
    event_image
};