export const RouteNames = {
    login:"/",
    signup:"/signup",
    forgot_password:"/forgot-password",
    reset_password:"/reset-password",
    dashboard:{
        home:"/dashboard"
    }
};